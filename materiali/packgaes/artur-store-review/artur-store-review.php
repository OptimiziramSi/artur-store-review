<?php
/**
* Plugin Name: Artur
* Description: External service artur.com
* Plugin URI: http://wordpress.org/plugins/artur
* Author: Matevž Novak - optimiziram.si
* Author URI: http://optimiziram.si
* Version: 1.0
* License: GPL2
* Text Domain: arturstorereview
* Domain Path: languages
*/

/*
Copyright (C) 2017  Matevž Novak - optimiziram.si  matevz@optimiziram.si

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * Snippet Wordpress Plugin Boilerplate based on:
 *
 * - https://github.com/purplefish32/sublime-text-2-wordpress/blob/master/Snippets/Plugin_Head.sublime-snippet
 * - http://wordpress.stackexchange.com/questions/25910/uninstall-activate-deactivate-a-plugin-typical-features-how-to/25979#25979
 *
 * By default the option to uninstall the plugin is disabled,
 * to use uncomment or remove if not used.
 *
 * This Template does not have the necessary code for use in multisite. 
 *
 * Also delete this comment block is unnecessary once you have read.
 *
 * Version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define('ARTURSTOREREVIEW_VERSION', '1.0');
define('ARTURSTOREREVIEW_FILE', __FILE__);
define('ARTURSTOREREVIEW_PATH', dirname(ARTURSTOREREVIEW_FILE));

require_once(ARTURSTOREREVIEW_PATH . '/includes/settings.php');
require_once(ARTURSTOREREVIEW_PATH . '/includes/cron.php');
require_once(ARTURSTOREREVIEW_PATH . '/includes/api.php');
require_once(ARTURSTOREREVIEW_PATH . '/includes/woocommerce.php');

add_action( 'plugins_loaded', array( 'ArturStoreReview', 'getInstance' ) );
register_activation_hook( __FILE__, array( 'ArturStoreReview', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'ArturStoreReview', 'deactivate' ) );
// register_uninstall_hook( __FILE__, array( 'ArturPlugin', 'uninstall' ) );

class ArturStoreReview {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {

		// load translations
		$this->load_textdomain();

		// init settings
		ArturStoreReview_Settings::getInstance();

		// init cron
		ArturStoreReview_Cron::getInstance();

		// add woocommerce support
		ArturStoreReview_Woocommerce::getInstance();

	}

	public function load_textdomain(){
		load_plugin_textdomain( 'arturstorereview', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
	}

	/*************************************************
			UTILS - START
	*************************************************/

	public static function is_plugin_active($plugin_name){
		if(!function_exists('is_plugin_active')){
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		return is_plugin_active($plugin_name);
	}
	
	/*************************************************
			UTILS - END
	*************************************************/


	public static function activate() {}

	public static function deactivate() {}

	// public static function uninstall() {
	// 	if ( __FILE__ != WP_UNINSTALL_PLUGIN )
	// 		return;

		
	// 	// settings
	// 	$settings = ArturStoreReview_Settings::getInstance();

	// 	// preserve data if requested so
	// 	if( ! $settings->bool('preserve_data_on_uninstall') ){

	// 		remove_option( ArturStoreReview_Cron::OPTION );
	// 		remove_option( ArturStoreReview_Settings::SETTINGS );
	// 		remove_option( ArturStoreReview_Settings::MESSAGES );

	// 	}

	// 	// maybe remove woocommerce artur store review meta fields ?
	// 	// TODO
		
		
	// 	// remove any scheduled crons
	// 	wp_clear_scheduled_hook( ArturStoreReview_Cron::ACTION );
	// }
}
