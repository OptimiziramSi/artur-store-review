<?php

class ArturStoreReview_Cron {

	const ACTION = 'arturstorereview_cron';
	const OPTION = 'arturstorereview_cron_option';

	private $cron_started_time = false;

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {

		add_action( 'arturstorereview_test_settings', array( $this, 'test_settings' ) );
		
		// comment out when done developing
		add_action( 'arturstorereview_test_settings', array( $this, 'dev_function' ) );

		add_filter( 'cron_schedules', array( $this, 'add_schedule_intervals' ) );

		add_action( self::ACTION, array( $this, 'cron_started' ), 1 );
		add_action( self::ACTION, array( $this, 'cron_ended' ), 99999 );
	}

	public function dev_function() {
		// trigger cron on settings save
		do_action( ArturStoreReview_Cron::ACTION );
	}

	public function test_settings()
	{
		// settings
		$settings = ArturStoreReview_Settings::getInstance();

		// if cron is already scheduled
		// but plugin is disabled or defered sending is disabled
		if( wp_get_schedule( self::ACTION ) && ( ! $settings->bool('enabled') || ! $settings->bool('defered_sending_enabled') ) ){
			
			// remove scheduled event
			wp_clear_scheduled_hook( self::ACTION );

			// and trigger it now
			do_action( self::ACTION );
		}

		// if cron is already scheduled but not for the right interval
		if( ( $schedule = wp_get_schedule( self::ACTION ) ) && $schedule != $settings->get('defered_sending_interval') ){
			// remove scheduled event
			wp_clear_scheduled_hook( self::ACTION );
		}

		// if plugin is enabled
		// and defered sending is enabled
		// but cron is not scheduled yet
		if( $settings->bool('enabled') && $settings->bool('defered_sending_enabled') && ! wp_next_scheduled( self::ACTION ) ){
			// schedule cron
			wp_schedule_event( time(), $settings->get('defered_sending_interval'), self::ACTION );
		}
	}

	public function add_schedule_intervals( $schedules ) {

		// settings
		$settings = ArturStoreReview_Settings::getInstance();

		// intervals
		$intervals = $settings->get_defered_sending_interval_options();

		// itterate intervals
		foreach ( $intervals as $interval => $interval_label ) {
			$interval_timespan = intval( $interval ) * 60;
			
			if( ! isset( $schedules[$interval] ) ){

				$schedules[ $interval ] = array(
					'interval' => $interval_timespan,
					'display' => $interval_label,
				);

			}
		}

		return $schedules;
	}

	public function cron_started() {
		$this->cron_started_time = time();
	}

	public function cron_ended() {
		// if no start time, do not log
		if( ! $this->cron_started_time )
			return;

		// get time needed
		$time_needed = time() - $this->cron_started_time;
		if( 0 === $time_needed )
			$time_needed = 1;

		// get number of sent invitations
		// TODO
		$requests_sent = 0;
		
		$cron_log = array(
			'started' => date( 'Y-m-d H:i:s', $this->cron_started_time ),
			'time_needed' => $time_needed,
			'requests_sent' => $requests_sent,
		);

		update_option( self::OPTION, $cron_log );
	}

	public function get_last_run_log(){
		$log = get_option( self::OPTION, false );

		return shortcode_atts(
			array(
				'started' => '/',
				'time_needed' => '/',
				'requests_sent' => '/',
			),
			$log
		);
	}

}